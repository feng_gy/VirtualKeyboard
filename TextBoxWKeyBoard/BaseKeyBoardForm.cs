﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TextBoxWKeyBoard
{
    public partial class BaseKeyBoardForm : CCWin.CCSkinMain
    {
        public BaseKeyBoardForm()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.TopMost = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Text = "";
            this.Load += new EventHandler(BaseKayBoardForm_Load);
        }

        private void BaseKayBoardForm_Load(object sender, EventArgs e)
        {
            Application.AddMessageFilter(new PopupWindowHelperMessageFilter(this, _textboxr));
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams ret = base.CreateParams;
                ret.Style = (int)Flags.WindowStyles.WS_THICKFRAME | (int)Flags.WindowStyles.WS_CHILD;
                ret.ExStyle |= (int)Flags.WindowStyles.WS_EX_NOACTIVATE | (int)Flags.WindowStyles.WS_EX_TOOLWINDOW;
                ret.X = this.Location.X;
                ret.Y = this.Location.Y + 5;
                return ret;
            }
        }
        private Control _textboxr;

        public Control TextBox
        {
            get
            {
                return _textboxr;
            }
            set
            {
                _textboxr = value;
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BaseKeyBoardForm
            // 
            this.BackColor = System.Drawing.Color.White;
            this.CanResize = false;
            this.CaptionHeight = 4;
            this.ClientSize = new System.Drawing.Size(327, 301);
            this.ControlBox = false;
            this.ForeColor = System.Drawing.Color.Black;
            this.Mobile = CCWin.MobileStyle.None;
            this.Name = "BaseKeyBoardForm";
            this.Radius = 10;
            this.ShadowWidth = 8;
            this.ShowDrawIcon = false;
            this.ShowIcon = false;
            this.ShowSystemMenu = true;
            this.Text = "";
            this.TitleCenter = true;
            this.TitleSuitColor = true;
            this.ResumeLayout(false);

        }
    }
}
