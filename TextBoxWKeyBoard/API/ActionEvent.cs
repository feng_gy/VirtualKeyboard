﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TextBoxWKeyBoard.API
{
    public class ActionEvent
    {
        /// <summary>
        /// 自定义键盘动作
        /// </summary>
        /// <param name="keyBoardAction"></param>
        public delegate void KeyBoardCustomAction(KeyBoardCustomActionEnum keyBoardAction);
        public static event KeyBoardCustomAction KeyBoardCustomEvent;

        /// <summary>
        /// 转发自定义键盘动作
        /// </summary>
        /// <param name="keyBoardAction"></param>
        public static void DispatherKeyBoardAction(KeyBoardCustomActionEnum keyBoardAction)
        {
            if (KeyBoardCustomEvent != null)
            {
                KeyBoardCustomEvent(keyBoardAction);
            }
        }

        /// <summary>
        /// 清空事件
        /// </summary>
        public void ClearEvent()
        {
            KeyBoardCustomEvent = null;
        }
    }
}
