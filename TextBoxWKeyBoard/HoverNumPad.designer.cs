﻿namespace TextBoxWKeyBoard
{
    partial class HoverNumPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.button2 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.button3 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard1 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard2 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard3 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard4 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard5 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard6 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard7 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard8 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.btn_Clear = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard10 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.buttonWKeyBoard11 = new TextBoxWKeyBoard.ButtonWKeyBoard();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.button1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.button1.DownBack = null;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("宋体", 20F);
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.IsPressed = false;
            this.button1.KeyCode = 0;
            this.button1.Location = new System.Drawing.Point(11, 129);
            this.button1.MouseBack = null;
            this.button1.Name = "button1";
            this.button1.NormalText = null;
            this.button1.NormlBack = null;
            this.button1.Radius = 10;
            this.button1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.button1.ShiftText = null;
            this.button1.Size = new System.Drawing.Size(59, 54);
            this.button1.TabIndex = 0;
            this.button1.Tag = "1";
            this.button1.Text = "1";
            this.button1.UnNumLockKeyCode = 0;
            this.button1.UnNumLockText = null;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.button2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.button2.DownBack = null;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("宋体", 20F);
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.IsPressed = false;
            this.button2.KeyCode = 0;
            this.button2.Location = new System.Drawing.Point(76, 129);
            this.button2.MouseBack = null;
            this.button2.Name = "button2";
            this.button2.NormalText = null;
            this.button2.NormlBack = null;
            this.button2.Radius = 10;
            this.button2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.button2.ShiftText = null;
            this.button2.Size = new System.Drawing.Size(59, 54);
            this.button2.TabIndex = 1;
            this.button2.Tag = "2";
            this.button2.Text = "2";
            this.button2.UnNumLockKeyCode = 0;
            this.button2.UnNumLockText = null;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.button3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.button3.DownBack = null;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("宋体", 20F);
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.IsPressed = false;
            this.button3.KeyCode = 0;
            this.button3.Location = new System.Drawing.Point(141, 129);
            this.button3.MouseBack = null;
            this.button3.Name = "button3";
            this.button3.NormalText = null;
            this.button3.NormlBack = null;
            this.button3.Radius = 10;
            this.button3.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.button3.ShiftText = null;
            this.button3.Size = new System.Drawing.Size(59, 54);
            this.button3.TabIndex = 2;
            this.button3.Tag = "3";
            this.button3.Text = "3";
            this.button3.UnNumLockKeyCode = 0;
            this.button3.UnNumLockText = null;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard1
            // 
            this.buttonWKeyBoard1.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard1.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard1.DownBack = null;
            this.buttonWKeyBoard1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard1.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard1.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard1.IsPressed = false;
            this.buttonWKeyBoard1.KeyCode = 0;
            this.buttonWKeyBoard1.Location = new System.Drawing.Point(11, 10);
            this.buttonWKeyBoard1.MouseBack = null;
            this.buttonWKeyBoard1.Name = "buttonWKeyBoard1";
            this.buttonWKeyBoard1.NormalText = null;
            this.buttonWKeyBoard1.NormlBack = null;
            this.buttonWKeyBoard1.Radius = 10;
            this.buttonWKeyBoard1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard1.ShiftText = null;
            this.buttonWKeyBoard1.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard1.TabIndex = 0;
            this.buttonWKeyBoard1.Tag = "7";
            this.buttonWKeyBoard1.Text = "7";
            this.buttonWKeyBoard1.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard1.UnNumLockText = null;
            this.buttonWKeyBoard1.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard2
            // 
            this.buttonWKeyBoard2.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard2.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard2.DownBack = null;
            this.buttonWKeyBoard2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard2.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard2.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard2.IsPressed = false;
            this.buttonWKeyBoard2.KeyCode = 0;
            this.buttonWKeyBoard2.Location = new System.Drawing.Point(76, 10);
            this.buttonWKeyBoard2.MouseBack = null;
            this.buttonWKeyBoard2.Name = "buttonWKeyBoard2";
            this.buttonWKeyBoard2.NormalText = null;
            this.buttonWKeyBoard2.NormlBack = null;
            this.buttonWKeyBoard2.Radius = 10;
            this.buttonWKeyBoard2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard2.ShiftText = null;
            this.buttonWKeyBoard2.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard2.TabIndex = 1;
            this.buttonWKeyBoard2.Tag = "8";
            this.buttonWKeyBoard2.Text = "8";
            this.buttonWKeyBoard2.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard2.UnNumLockText = null;
            this.buttonWKeyBoard2.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard3
            // 
            this.buttonWKeyBoard3.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard3.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard3.DownBack = null;
            this.buttonWKeyBoard3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard3.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard3.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard3.IsPressed = false;
            this.buttonWKeyBoard3.KeyCode = 0;
            this.buttonWKeyBoard3.Location = new System.Drawing.Point(141, 10);
            this.buttonWKeyBoard3.MouseBack = null;
            this.buttonWKeyBoard3.Name = "buttonWKeyBoard3";
            this.buttonWKeyBoard3.NormalText = null;
            this.buttonWKeyBoard3.NormlBack = null;
            this.buttonWKeyBoard3.Radius = 10;
            this.buttonWKeyBoard3.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard3.ShiftText = null;
            this.buttonWKeyBoard3.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard3.TabIndex = 2;
            this.buttonWKeyBoard3.Tag = "9";
            this.buttonWKeyBoard3.Text = "9";
            this.buttonWKeyBoard3.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard3.UnNumLockText = null;
            this.buttonWKeyBoard3.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard4
            // 
            this.buttonWKeyBoard4.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard4.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard4.DownBack = null;
            this.buttonWKeyBoard4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard4.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard4.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard4.IsPressed = false;
            this.buttonWKeyBoard4.KeyCode = 0;
            this.buttonWKeyBoard4.Location = new System.Drawing.Point(11, 69);
            this.buttonWKeyBoard4.MouseBack = null;
            this.buttonWKeyBoard4.Name = "buttonWKeyBoard4";
            this.buttonWKeyBoard4.NormalText = null;
            this.buttonWKeyBoard4.NormlBack = null;
            this.buttonWKeyBoard4.Radius = 10;
            this.buttonWKeyBoard4.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard4.ShiftText = null;
            this.buttonWKeyBoard4.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard4.TabIndex = 0;
            this.buttonWKeyBoard4.Tag = "4";
            this.buttonWKeyBoard4.Text = "4";
            this.buttonWKeyBoard4.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard4.UnNumLockText = null;
            this.buttonWKeyBoard4.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard5
            // 
            this.buttonWKeyBoard5.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard5.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard5.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard5.DownBack = null;
            this.buttonWKeyBoard5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard5.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard5.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard5.IsPressed = false;
            this.buttonWKeyBoard5.KeyCode = 0;
            this.buttonWKeyBoard5.Location = new System.Drawing.Point(76, 69);
            this.buttonWKeyBoard5.MouseBack = null;
            this.buttonWKeyBoard5.Name = "buttonWKeyBoard5";
            this.buttonWKeyBoard5.NormalText = null;
            this.buttonWKeyBoard5.NormlBack = null;
            this.buttonWKeyBoard5.Radius = 10;
            this.buttonWKeyBoard5.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard5.ShiftText = null;
            this.buttonWKeyBoard5.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard5.TabIndex = 1;
            this.buttonWKeyBoard5.Tag = "5";
            this.buttonWKeyBoard5.Text = "5";
            this.buttonWKeyBoard5.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard5.UnNumLockText = null;
            this.buttonWKeyBoard5.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard6
            // 
            this.buttonWKeyBoard6.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard6.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard6.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard6.DownBack = null;
            this.buttonWKeyBoard6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard6.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard6.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard6.IsPressed = false;
            this.buttonWKeyBoard6.KeyCode = 0;
            this.buttonWKeyBoard6.Location = new System.Drawing.Point(141, 69);
            this.buttonWKeyBoard6.MouseBack = null;
            this.buttonWKeyBoard6.Name = "buttonWKeyBoard6";
            this.buttonWKeyBoard6.NormalText = null;
            this.buttonWKeyBoard6.NormlBack = null;
            this.buttonWKeyBoard6.Radius = 10;
            this.buttonWKeyBoard6.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard6.ShiftText = null;
            this.buttonWKeyBoard6.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard6.TabIndex = 2;
            this.buttonWKeyBoard6.Tag = "6";
            this.buttonWKeyBoard6.Text = "6";
            this.buttonWKeyBoard6.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard6.UnNumLockText = null;
            this.buttonWKeyBoard6.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard7
            // 
            this.buttonWKeyBoard7.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard7.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard7.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard7.DownBack = null;
            this.buttonWKeyBoard7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard7.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard7.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard7.IsPressed = false;
            this.buttonWKeyBoard7.KeyCode = 0;
            this.buttonWKeyBoard7.Location = new System.Drawing.Point(11, 189);
            this.buttonWKeyBoard7.MouseBack = null;
            this.buttonWKeyBoard7.Name = "buttonWKeyBoard7";
            this.buttonWKeyBoard7.NormalText = null;
            this.buttonWKeyBoard7.NormlBack = null;
            this.buttonWKeyBoard7.Radius = 10;
            this.buttonWKeyBoard7.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard7.ShiftText = null;
            this.buttonWKeyBoard7.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard7.TabIndex = 0;
            this.buttonWKeyBoard7.Tag = "0";
            this.buttonWKeyBoard7.Text = "0";
            this.buttonWKeyBoard7.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard7.UnNumLockText = null;
            this.buttonWKeyBoard7.UseVisualStyleBackColor = true;
            // 
            // buttonWKeyBoard8
            // 
            this.buttonWKeyBoard8.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard8.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard8.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard8.DownBack = null;
            this.buttonWKeyBoard8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard8.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard8.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard8.IsPressed = false;
            this.buttonWKeyBoard8.KeyCode = 0;
            this.buttonWKeyBoard8.Location = new System.Drawing.Point(76, 189);
            this.buttonWKeyBoard8.MouseBack = null;
            this.buttonWKeyBoard8.Name = "buttonWKeyBoard8";
            this.buttonWKeyBoard8.NormalText = null;
            this.buttonWKeyBoard8.NormlBack = null;
            this.buttonWKeyBoard8.Radius = 10;
            this.buttonWKeyBoard8.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard8.ShiftText = null;
            this.buttonWKeyBoard8.Size = new System.Drawing.Size(124, 54);
            this.buttonWKeyBoard8.TabIndex = 1;
            this.buttonWKeyBoard8.Tag = "{BKSP}";
            this.buttonWKeyBoard8.Text = "Back";
            this.buttonWKeyBoard8.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard8.UnNumLockText = null;
            this.buttonWKeyBoard8.UseVisualStyleBackColor = true;
            // 
            // btn_Clear
            // 
            this.btn_Clear.BackColor = System.Drawing.Color.Transparent;
            this.btn_Clear.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.btn_Clear.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Clear.DownBack = null;
            this.btn_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Clear.Font = new System.Drawing.Font("宋体", 20F);
            this.btn_Clear.ForeColor = System.Drawing.Color.Black;
            this.btn_Clear.IsPressed = false;
            this.btn_Clear.KeyCode = 0;
            this.btn_Clear.Location = new System.Drawing.Point(206, 130);
            this.btn_Clear.MouseBack = null;
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.NormalText = null;
            this.btn_Clear.NormlBack = null;
            this.btn_Clear.Radius = 10;
            this.btn_Clear.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.btn_Clear.ShiftText = null;
            this.btn_Clear.Size = new System.Drawing.Size(59, 113);
            this.btn_Clear.TabIndex = 3;
            this.btn_Clear.Text = "清空";
            this.btn_Clear.UnNumLockKeyCode = 0;
            this.btn_Clear.UnNumLockText = null;
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // buttonWKeyBoard10
            // 
            this.buttonWKeyBoard10.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard10.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard10.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard10.DownBack = null;
            this.buttonWKeyBoard10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard10.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard10.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard10.IsPressed = false;
            this.buttonWKeyBoard10.KeyCode = 0;
            this.buttonWKeyBoard10.Location = new System.Drawing.Point(206, 11);
            this.buttonWKeyBoard10.MouseBack = null;
            this.buttonWKeyBoard10.Name = "buttonWKeyBoard10";
            this.buttonWKeyBoard10.NormalText = null;
            this.buttonWKeyBoard10.NormlBack = null;
            this.buttonWKeyBoard10.Radius = 10;
            this.buttonWKeyBoard10.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard10.ShiftText = null;
            this.buttonWKeyBoard10.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard10.TabIndex = 4;
            this.buttonWKeyBoard10.Tag = "{+}";
            this.buttonWKeyBoard10.Text = "+";
            this.buttonWKeyBoard10.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard10.UnNumLockText = null;
            this.buttonWKeyBoard10.UseVisualStyleBackColor = true;
            this.buttonWKeyBoard10.Click += new System.EventHandler(this.buttonWKeyBoard10_Click);
            // 
            // buttonWKeyBoard11
            // 
            this.buttonWKeyBoard11.BackColor = System.Drawing.Color.Transparent;
            this.buttonWKeyBoard11.BaseColor = System.Drawing.Color.LightSteelBlue;
            this.buttonWKeyBoard11.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonWKeyBoard11.DownBack = null;
            this.buttonWKeyBoard11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWKeyBoard11.Font = new System.Drawing.Font("宋体", 20F);
            this.buttonWKeyBoard11.ForeColor = System.Drawing.Color.Black;
            this.buttonWKeyBoard11.IsPressed = false;
            this.buttonWKeyBoard11.KeyCode = 0;
            this.buttonWKeyBoard11.Location = new System.Drawing.Point(206, 70);
            this.buttonWKeyBoard11.MouseBack = null;
            this.buttonWKeyBoard11.Name = "buttonWKeyBoard11";
            this.buttonWKeyBoard11.NormalText = null;
            this.buttonWKeyBoard11.NormlBack = null;
            this.buttonWKeyBoard11.Radius = 10;
            this.buttonWKeyBoard11.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.buttonWKeyBoard11.ShiftText = null;
            this.buttonWKeyBoard11.Size = new System.Drawing.Size(59, 54);
            this.buttonWKeyBoard11.TabIndex = 5;
            this.buttonWKeyBoard11.Tag = "{-}";
            this.buttonWKeyBoard11.Text = "-";
            this.buttonWKeyBoard11.UnNumLockKeyCode = 0;
            this.buttonWKeyBoard11.UnNumLockText = null;
            this.buttonWKeyBoard11.UseVisualStyleBackColor = true;
            this.buttonWKeyBoard11.Click += new System.EventHandler(this.buttonWKeyBoard11_Click);
            // 
            // HoverNumPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 260);
            this.Controls.Add(this.buttonWKeyBoard11);
            this.Controls.Add(this.buttonWKeyBoard10);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.buttonWKeyBoard6);
            this.Controls.Add(this.buttonWKeyBoard3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buttonWKeyBoard5);
            this.Controls.Add(this.buttonWKeyBoard4);
            this.Controls.Add(this.buttonWKeyBoard2);
            this.Controls.Add(this.buttonWKeyBoard8);
            this.Controls.Add(this.buttonWKeyBoard1);
            this.Controls.Add(this.buttonWKeyBoard7);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "HoverNumPad";
            this.Load += new System.EventHandler(this.HoverNumPad_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ButtonWKeyBoard button1;
        private ButtonWKeyBoard button2;
        private ButtonWKeyBoard button3;
        private ButtonWKeyBoard buttonWKeyBoard1;
        private ButtonWKeyBoard buttonWKeyBoard2;
        private ButtonWKeyBoard buttonWKeyBoard3;
        private ButtonWKeyBoard buttonWKeyBoard4;
        private ButtonWKeyBoard buttonWKeyBoard5;
        private ButtonWKeyBoard buttonWKeyBoard6;
        private ButtonWKeyBoard buttonWKeyBoard7;
        private ButtonWKeyBoard buttonWKeyBoard8;
        private ButtonWKeyBoard btn_Clear;
        private ButtonWKeyBoard buttonWKeyBoard10;
        private ButtonWKeyBoard buttonWKeyBoard11;
    }
}