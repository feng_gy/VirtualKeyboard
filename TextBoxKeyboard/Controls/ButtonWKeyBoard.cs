﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace TextBoxWKeyBoard
{
    public partial class ButtonKeyBoard : Button
    {
        public ButtonKeyBoard()
        {
            this.Click += new EventHandler(ButtonWKeyBoard_Click);
        }

        private void ButtonWKeyBoard_Click(object sender, EventArgs e)
        {
            object obj = ((Button)sender).Tag;
            if (obj == null)
            {
                return;
            }
            SendKeys.Send(obj.ToString());
        }
    }
}
