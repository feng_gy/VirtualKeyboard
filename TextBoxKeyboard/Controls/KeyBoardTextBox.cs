﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TextBoxWKeyBoard.Controls
{
    public partial class KeyBoardTextBox : UserControl
    {
        public KeyBoardTextBox()
        {
            InitializeComponent();
        }

        public TexBoxWKeyBoard TextBox
        {
            get { return this.textBox1; }
        }

        private void KeyBoardTextBox_Paint(object sender, PaintEventArgs e)
        {
            this.pictureBox1.Width = Convert.ToInt32(this.Width * 0.15);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.textBox1.ShowKeyBoard();
        }
    }
}
