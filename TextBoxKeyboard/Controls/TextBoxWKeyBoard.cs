﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using TextBoxWKeyBoard.API;

namespace TextBoxWKeyBoard
{
    public class TexBoxWKeyBoard : DevComponents.DotNetBar.Controls.TextBoxX
    {
        private Form HK;
        public TexBoxWKeyBoard()
        {
            this.MaxLength = 20;
            this.Leave += new EventHandler(TexBoxWKeyBoard_Leave);
            this.KeyDown += new KeyEventHandler(TextBoxWKeyBoard_KeyDown);
            this.KeyPress += new KeyPressEventHandler(TextBoxWKeyBoard_KeyPress);
            ActionEvent.KeyBoardCustomEvent += new ActionEvent.KeyBoardCustomAction(ActionEvent_KeyBoardCustomEvent);
        }

        public void ShowKeyBoard()
        {
            if (HK == null)
            {
                Point location = this.Parent.PointToScreen(new Point(this.Left, this.Bottom));
                if (TextEntryMode == EntryMode.Standard)
                {
                    HK = new HoverKeyboard();
                    ((HoverKeyboard)HK).TextBox = this;
                    location = new Point(Convert.ToInt32((Screen.PrimaryScreen.WorkingArea.Width - HK.Width) / 2), location.Y);
                }
                else
                {
                    HK = new HoverNumPad();
                    ((HoverNumPad)HK).TextBox = this;
                }
                HK.Location = location;
                HK.FormClosed += new FormClosedEventHandler(HK_FormClosed);
                HK.Show();
                this.Focus();
            }
        }

        public void CloseKeyBoard()
        {
            if (HK != null)
            {
                HK.Close();
                HK = null;
            }
        }
        private void ActionEvent_KeyBoardCustomEvent(KeyBoardCustomActionEnum keyBoardAction)
        {
            if (TextEntryMode == EntryMode.Numeric)
            {
                string content = this.Text;
                decimal result = decimal.MinusOne;
                if (decimal.TryParse(content, out result))
                {
                    switch (keyBoardAction)
                    {
                        case KeyBoardCustomActionEnum.加:

                            this.Text = (result + 1).ToString();
                            break;
                        case KeyBoardCustomActionEnum.减:

                            this.Text = (result - 1).ToString();
                            break;
                    }
                    this.SelectionStart = this.Text.Length;
                }
            }
        }

        void TexBoxWKeyBoard_Leave(object sender, EventArgs e)
        {
            CloseKeyBoard();
        }

        void HK_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseKeyBoard();
        }

        public enum EntryMode
        {
            Standard,
            Numeric
        }


        private EntryMode _TextEntryMode = EntryMode.Standard;

        public EntryMode TextEntryMode
        {
            get
            {
                return _TextEntryMode;
            }
            set
            {
                _TextEntryMode = value;
            }
        }

        #region numericMode
        //This is secion is based on Capturing numeric input in a TextBox by ddanbe on DaniWeb
        //http://www.daniweb.com/code/snippet217265.html
        //Its simple enough, but danny did a great job so I thought I would give him some credit.

        bool NumberEntered = false;

        //Check if key entered is "numeric".
        private bool CheckIfNumericKey(Keys K, bool isDecimalPoint)
        {
            if (K == Keys.Back) //backspace?
                return true;
            else if (K == Keys.OemPeriod || K == Keys.Decimal)  //decimal point?
                return isDecimalPoint ? false : true;       //or: return !isDecimalPoint
            else if ((K >= Keys.D0) && (K <= Keys.D9))      //digit from top of keyboard?
                return true;
            else if ((K >= Keys.NumPad0) && (K <= Keys.NumPad9))    //digit from keypad?
                return true;
            else
                return false;   //no "numeric" key
        }

        private void TextBoxWKeyBoard_KeyDown(object sender, KeyEventArgs e)
        {
            if (TextEntryMode == EntryMode.Numeric)
            {
                //Get our textbox.
                TextBox Tbx = (TextBox)sender;
                // Initialize the flag.
                NumberEntered = CheckIfNumericKey(e.KeyCode, Tbx.Text.Contains("."));
            }
        }

        // This event occurs after the KeyDown event and can be used to prevent
        // characters from entering the control.
        private void TextBoxWKeyBoard_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (TextEntryMode == EntryMode.Numeric)
            {
                // Check for the flag being set in the KeyDown event.
                if (NumberEntered == false)
                {
                    // Stop the character from being entered into the control since it is non-numerical.
                    e.Handled = true;
                }
            }

        }
        #endregion
    }
}
