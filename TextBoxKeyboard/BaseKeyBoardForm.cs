﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TextBoxWKeyBoard
{
    public partial class BaseKeyBoardForm : CCWin.CCSkinMain
    {
        public BaseKeyBoardForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams ret = base.CreateParams;
                ret.Style = (int)Flags.WindowStyles.WS_THICKFRAME | (int)Flags.WindowStyles.WS_CHILD;
                ret.ExStyle |= (int)Flags.WindowStyles.WS_EX_NOACTIVATE | (int)Flags.WindowStyles.WS_EX_TOOLWINDOW;
                ret.X = this.Location.X;
                ret.Y = this.Location.Y + 5;
                return ret;
            }
        }
        private Control _textboxr;

        public Control TextBox
        {
            get
            {
                return _textboxr;
            }
            set
            {
                _textboxr = value;
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BaseKeyBoardForm
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(413, 301);
            this.ControlBox = false;
            this.ForeColor = System.Drawing.Color.Black;
            this.Mobile = CCWin.MobileStyle.None;
            this.Name = "BaseKeyBoardForm";
            this.Radius = 10;
            this.ShowDrawIcon = false;
            this.ShowIcon = false;
            this.Text = "";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.BaseKeyBoardForm_Load);
            this.ResumeLayout(false);

        }

        private void BaseKeyBoardForm_Load(object sender, EventArgs e)
        {
            Application.AddMessageFilter(new PopupWindowHelperMessageFilter(this, _textboxr));
        }
    }
}
