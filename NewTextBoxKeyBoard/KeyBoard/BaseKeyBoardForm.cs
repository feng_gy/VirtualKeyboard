﻿using NewTextBoxKeyBoard;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TextBoxWKeyBoard
{
    public partial class BaseKeyBoardForm : Form
    {
        internal HsTextBox textBox { get; set; }

        public BaseKeyBoardForm()
        {
            InitializeComponent();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        public new void Close()
        {
            textBox.CloseDropPanel();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            PostMessage(textBox.Handle, msg.Msg, (int)keyData, (int)msg.LParam);
            return true;
        }

        [DllImport("user32.dll")]
        public static extern void PostMessage(IntPtr hWnd, int msg, int wParam, int lParam);
        private readonly int MOUSEEVENTF_LEFTDOWN = 0x2;
        private readonly int MOUSEEVENTF_LEFTUP = 0x4;
        [DllImport("user32 ")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BaseKeyBoardForm
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(413, 301);
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BaseKeyBoardForm";
            this.ShowIcon = false;
            this.TopMost = true;
            this.ResumeLayout(false);

        }
    }
}