﻿using System.Windows.Forms;
namespace TextBoxWKeyBoard
{
    partial class HoverKeyboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Enter = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button1 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button3 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button4 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button5 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button6 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button7 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button8 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button9 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button10 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button11 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.btn_Clear = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button13 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button14 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button15 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button16 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button17 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button18 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button19 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button20 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button21 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button22 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button23 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button24 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button25 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button26 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button27 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button28 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button29 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button30 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button31 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button32 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button33 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button34 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button35 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button36 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button37 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.button38 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.SuspendLayout();
            // 
            // btn_Enter
            // 
            this.btn_Enter.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_Enter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Enter.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.btn_Enter.ForeColor = System.Drawing.Color.White;
            this.btn_Enter.Location = new System.Drawing.Point(628, 168);
            this.btn_Enter.Name = "btn_Enter";
            this.btn_Enter.Size = new System.Drawing.Size(164, 46);
            this.btn_Enter.TabIndex = 6;
            this.btn_Enter.Text = "回车";
            this.btn_Enter.UseVisualStyleBackColor = false;
            this.btn_Enter.Click += new System.EventHandler(this.btn_Enter_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gray;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(16, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 46);
            this.button1.TabIndex = 7;
            this.button1.Tag = 1;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Gray;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(83, 11);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(50, 46);
            this.button3.TabIndex = 7;
            this.button3.Tag = 2;
            this.button3.Text = "2";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gray;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(155, 11);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 46);
            this.button4.TabIndex = 7;
            this.button4.Tag = 3;
            this.button4.Text = "3";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gray;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(228, 11);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 46);
            this.button5.TabIndex = 7;
            this.button5.Tag = 4;
            this.button5.Text = "4";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Gray;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(301, 11);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(50, 46);
            this.button6.TabIndex = 7;
            this.button6.Tag = 5;
            this.button6.Text = "5";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Gray;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(371, 11);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(50, 46);
            this.button7.TabIndex = 7;
            this.button7.Tag = 6;
            this.button7.Text = "6";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Gray;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(442, 12);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 46);
            this.button8.TabIndex = 7;
            this.button8.Tag = 7;
            this.button8.Text = "7";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Gray;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(512, 12);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 46);
            this.button9.TabIndex = 7;
            this.button9.Tag = 8;
            this.button9.Text = "8";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Gray;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Location = new System.Drawing.Point(582, 12);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(50, 46);
            this.button10.TabIndex = 7;
            this.button10.Tag = 9;
            this.button10.Text = "9";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Gray;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(650, 12);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(50, 46);
            this.button11.TabIndex = 7;
            this.button11.Tag = 0;
            this.button11.Text = "0";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // btn_Clear
            // 
            this.btn_Clear.BackColor = System.Drawing.Color.DarkOrange;
            this.btn_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Clear.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.btn_Clear.ForeColor = System.Drawing.Color.White;
            this.btn_Clear.Location = new System.Drawing.Point(719, 12);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(74, 46);
            this.btn_Clear.TabIndex = 6;
            this.btn_Clear.Tag = "{BKSP}";
            this.btn_Clear.Text = "删除";
            this.btn_Clear.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Gray;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(29, 63);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(50, 46);
            this.button13.TabIndex = 7;
            this.button13.Tag = "Q";
            this.button13.Text = "Q";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Gray;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(96, 63);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(50, 46);
            this.button14.TabIndex = 7;
            this.button14.Tag = System.Windows.Forms.Keys.W;
            this.button14.Text = "W";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Gray;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Location = new System.Drawing.Point(168, 63);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(50, 46);
            this.button15.TabIndex = 7;
            this.button15.Tag = System.Windows.Forms.Keys.E;
            this.button15.Text = "E";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Gray;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Location = new System.Drawing.Point(241, 63);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(50, 46);
            this.button16.TabIndex = 7;
            this.button16.Tag = System.Windows.Forms.Keys.R;
            this.button16.Text = "R";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.Gray;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Location = new System.Drawing.Point(314, 63);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(50, 46);
            this.button17.TabIndex = 7;
            this.button17.Tag = System.Windows.Forms.Keys.T;
            this.button17.Text = "T";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.Gray;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(384, 63);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(50, 46);
            this.button18.TabIndex = 7;
            this.button18.Tag = System.Windows.Forms.Keys.Y;
            this.button18.Text = "Y";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.Gray;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button19.ForeColor = System.Drawing.Color.White;
            this.button19.Location = new System.Drawing.Point(455, 64);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(50, 46);
            this.button19.TabIndex = 7;
            this.button19.Tag = System.Windows.Forms.Keys.U;
            this.button19.Text = "U";
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.Gray;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Location = new System.Drawing.Point(525, 64);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(50, 46);
            this.button20.TabIndex = 7;
            this.button20.Tag = System.Windows.Forms.Keys.I;
            this.button20.Text = "I";
            this.button20.UseVisualStyleBackColor = false;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.Gray;
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button21.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button21.ForeColor = System.Drawing.Color.White;
            this.button21.Location = new System.Drawing.Point(595, 64);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(50, 46);
            this.button21.TabIndex = 7;
            this.button21.Tag = System.Windows.Forms.Keys.O;
            this.button21.Text = "O";
            this.button21.UseVisualStyleBackColor = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.Gray;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button22.ForeColor = System.Drawing.Color.White;
            this.button22.Location = new System.Drawing.Point(663, 64);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(50, 46);
            this.button22.TabIndex = 7;
            this.button22.Tag = System.Windows.Forms.Keys.P;
            this.button22.Text = "P";
            this.button22.UseVisualStyleBackColor = false;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.Gray;
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button23.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button23.ForeColor = System.Drawing.Color.White;
            this.button23.Location = new System.Drawing.Point(62, 115);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(50, 46);
            this.button23.TabIndex = 7;
            this.button23.Tag = System.Windows.Forms.Keys.A;
            this.button23.Text = "A";
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.Gray;
            this.button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button24.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button24.ForeColor = System.Drawing.Color.White;
            this.button24.Location = new System.Drawing.Point(129, 115);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(50, 46);
            this.button24.TabIndex = 7;
            this.button24.Tag = System.Windows.Forms.Keys.S;
            this.button24.Text = "S";
            this.button24.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.Gray;
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Location = new System.Drawing.Point(201, 115);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(50, 46);
            this.button25.TabIndex = 7;
            this.button25.Tag = System.Windows.Forms.Keys.D;
            this.button25.Text = "D";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.Gray;
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button26.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button26.ForeColor = System.Drawing.Color.White;
            this.button26.Location = new System.Drawing.Point(274, 115);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(50, 46);
            this.button26.TabIndex = 7;
            this.button26.Tag = System.Windows.Forms.Keys.F;
            this.button26.Text = "F";
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.Gray;
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button27.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button27.ForeColor = System.Drawing.Color.White;
            this.button27.Location = new System.Drawing.Point(347, 115);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(50, 46);
            this.button27.TabIndex = 7;
            this.button27.Tag = System.Windows.Forms.Keys.G;
            this.button27.Text = "G";
            this.button27.UseVisualStyleBackColor = false;
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.Color.Gray;
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button28.ForeColor = System.Drawing.Color.White;
            this.button28.Location = new System.Drawing.Point(417, 115);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(50, 46);
            this.button28.TabIndex = 7;
            this.button28.Tag = System.Windows.Forms.Keys.H;
            this.button28.Text = "H";
            this.button28.UseVisualStyleBackColor = false;
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.Gray;
            this.button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button29.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button29.ForeColor = System.Drawing.Color.White;
            this.button29.Location = new System.Drawing.Point(488, 116);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(50, 46);
            this.button29.TabIndex = 7;
            this.button29.Tag = System.Windows.Forms.Keys.J;
            this.button29.Text = "J";
            this.button29.UseVisualStyleBackColor = false;
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.Gray;
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button30.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Location = new System.Drawing.Point(558, 116);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(50, 46);
            this.button30.TabIndex = 7;
            this.button30.Tag = System.Windows.Forms.Keys.K;
            this.button30.Text = "K";
            this.button30.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.Color.Gray;
            this.button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button31.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button31.ForeColor = System.Drawing.Color.White;
            this.button31.Location = new System.Drawing.Point(628, 116);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(50, 46);
            this.button31.TabIndex = 7;
            this.button31.Tag = System.Windows.Forms.Keys.L;
            this.button31.Text = "L";
            this.button31.UseVisualStyleBackColor = false;
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.Gray;
            this.button32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button32.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button32.ForeColor = System.Drawing.Color.White;
            this.button32.Location = new System.Drawing.Point(102, 167);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(50, 46);
            this.button32.TabIndex = 7;
            this.button32.Tag = System.Windows.Forms.Keys.Z;
            this.button32.Text = "Z";
            this.button32.UseVisualStyleBackColor = false;
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.Gray;
            this.button33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button33.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button33.ForeColor = System.Drawing.Color.White;
            this.button33.Location = new System.Drawing.Point(169, 167);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(50, 46);
            this.button33.TabIndex = 7;
            this.button33.Tag = System.Windows.Forms.Keys.X;
            this.button33.Text = "X";
            this.button33.UseVisualStyleBackColor = false;
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.Gray;
            this.button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button34.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button34.ForeColor = System.Drawing.Color.White;
            this.button34.Location = new System.Drawing.Point(241, 167);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(50, 46);
            this.button34.TabIndex = 7;
            this.button34.Tag = System.Windows.Forms.Keys.C;
            this.button34.Text = "C";
            this.button34.UseVisualStyleBackColor = false;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.Color.Gray;
            this.button35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button35.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button35.ForeColor = System.Drawing.Color.White;
            this.button35.Location = new System.Drawing.Point(314, 167);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(50, 46);
            this.button35.TabIndex = 7;
            this.button35.Tag = System.Windows.Forms.Keys.V;
            this.button35.Text = "V";
            this.button35.UseVisualStyleBackColor = false;
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.Color.Gray;
            this.button36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button36.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button36.ForeColor = System.Drawing.Color.White;
            this.button36.Location = new System.Drawing.Point(387, 167);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(50, 46);
            this.button36.TabIndex = 7;
            this.button36.Tag = System.Windows.Forms.Keys.B;
            this.button36.Text = "B";
            this.button36.UseVisualStyleBackColor = false;
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.Gray;
            this.button37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button37.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button37.ForeColor = System.Drawing.Color.White;
            this.button37.Location = new System.Drawing.Point(457, 167);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(50, 46);
            this.button37.TabIndex = 7;
            this.button37.Tag = System.Windows.Forms.Keys.N;
            this.button37.Text = "N";
            this.button37.UseVisualStyleBackColor = false;
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.Gray;
            this.button38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button38.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button38.ForeColor = System.Drawing.Color.White;
            this.button38.Location = new System.Drawing.Point(528, 168);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(50, 46);
            this.button38.TabIndex = 7;
            this.button38.Tag = System.Windows.Forms.Keys.M;
            this.button38.Text = "M";
            this.button38.UseVisualStyleBackColor = false;
            // 
            // HoverKeyboard
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 229);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button38);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button37);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Enter);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "HoverKeyboard";
            this.ResumeLayout(false);

        }

        #endregion

        private ButtonKeyBoard btn_Enter;
        private ButtonKeyBoard button1;
        private ButtonKeyBoard button3;
        private ButtonKeyBoard button4;
        private ButtonKeyBoard button5;
        private ButtonKeyBoard button6;
        private ButtonKeyBoard button7;
        private ButtonKeyBoard button8;
        private ButtonKeyBoard button9;
        private ButtonKeyBoard button10;
        private ButtonKeyBoard button11;
        private ButtonKeyBoard btn_Clear;
        private ButtonKeyBoard button13;
        private ButtonKeyBoard button14;
        private ButtonKeyBoard button15;
        private ButtonKeyBoard button16;
        private ButtonKeyBoard button17;
        private ButtonKeyBoard button18;
        private ButtonKeyBoard button19;
        private ButtonKeyBoard button20;
        private ButtonKeyBoard button21;
        private ButtonKeyBoard button22;
        private ButtonKeyBoard button23;
        private ButtonKeyBoard button24;
        private ButtonKeyBoard button25;
        private ButtonKeyBoard button26;
        private ButtonKeyBoard button27;
        private ButtonKeyBoard button28;
        private ButtonKeyBoard button29;
        private ButtonKeyBoard button30;
        private ButtonKeyBoard button31;
        private ButtonKeyBoard button32;
        private ButtonKeyBoard button33;
        private ButtonKeyBoard button34;
        private ButtonKeyBoard button35;
        private ButtonKeyBoard button36;
        private ButtonKeyBoard button37;
        private ButtonKeyBoard button38;


    }
}