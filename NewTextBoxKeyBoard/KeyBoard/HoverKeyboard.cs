﻿using System;

namespace TextBoxWKeyBoard
{
    public partial class HoverKeyboard : BaseKeyBoardForm
    {
        public HoverKeyboard()
        {
            InitializeComponent();
        }

        private void btn_Enter_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}