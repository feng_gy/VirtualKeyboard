﻿namespace TextBoxWKeyBoard
{
    partial class HoverNumPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button13 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard9 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard12 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard13 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard14 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard15 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard16 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard17 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard18 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard20 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.btn_Enter = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard22 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.buttonKeyBoard23 = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.btn_Delete = new TextBoxWKeyBoard.ButtonKeyBoard();
            this.SuspendLayout();
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Gray;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(9, 10);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(50, 46);
            this.button13.TabIndex = 8;
            this.button13.Tag = "7";
            this.button13.Text = "7";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard9
            // 
            this.buttonKeyBoard9.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard9.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard9.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard9.Location = new System.Drawing.Point(65, 10);
            this.buttonKeyBoard9.Name = "buttonKeyBoard9";
            this.buttonKeyBoard9.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard9.TabIndex = 8;
            this.buttonKeyBoard9.Tag = "8";
            this.buttonKeyBoard9.Text = "8";
            this.buttonKeyBoard9.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard12
            // 
            this.buttonKeyBoard12.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard12.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard12.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard12.Location = new System.Drawing.Point(121, 10);
            this.buttonKeyBoard12.Name = "buttonKeyBoard12";
            this.buttonKeyBoard12.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard12.TabIndex = 8;
            this.buttonKeyBoard12.Tag = "9";
            this.buttonKeyBoard12.Text = "9";
            this.buttonKeyBoard12.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard13
            // 
            this.buttonKeyBoard13.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard13.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard13.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard13.Location = new System.Drawing.Point(177, 10);
            this.buttonKeyBoard13.Name = "buttonKeyBoard13";
            this.buttonKeyBoard13.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard13.TabIndex = 8;
            this.buttonKeyBoard13.Tag = "{+}";
            this.buttonKeyBoard13.Text = "+";
            this.buttonKeyBoard13.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard14
            // 
            this.buttonKeyBoard14.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard14.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard14.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard14.Location = new System.Drawing.Point(9, 62);
            this.buttonKeyBoard14.Name = "buttonKeyBoard14";
            this.buttonKeyBoard14.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard14.TabIndex = 8;
            this.buttonKeyBoard14.Tag = "4";
            this.buttonKeyBoard14.Text = "4";
            this.buttonKeyBoard14.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard15
            // 
            this.buttonKeyBoard15.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard15.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard15.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard15.Location = new System.Drawing.Point(65, 62);
            this.buttonKeyBoard15.Name = "buttonKeyBoard15";
            this.buttonKeyBoard15.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard15.TabIndex = 8;
            this.buttonKeyBoard15.Tag = "5";
            this.buttonKeyBoard15.Text = "5";
            this.buttonKeyBoard15.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard16
            // 
            this.buttonKeyBoard16.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard16.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard16.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard16.Location = new System.Drawing.Point(121, 62);
            this.buttonKeyBoard16.Name = "buttonKeyBoard16";
            this.buttonKeyBoard16.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard16.TabIndex = 8;
            this.buttonKeyBoard16.Tag = "6";
            this.buttonKeyBoard16.Text = "6";
            this.buttonKeyBoard16.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard17
            // 
            this.buttonKeyBoard17.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard17.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard17.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard17.Location = new System.Drawing.Point(177, 62);
            this.buttonKeyBoard17.Name = "buttonKeyBoard17";
            this.buttonKeyBoard17.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard17.TabIndex = 8;
            this.buttonKeyBoard17.Tag = "{-}";
            this.buttonKeyBoard17.Text = "-";
            this.buttonKeyBoard17.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard18
            // 
            this.buttonKeyBoard18.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard18.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard18.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard18.Location = new System.Drawing.Point(9, 114);
            this.buttonKeyBoard18.Name = "buttonKeyBoard18";
            this.buttonKeyBoard18.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard18.TabIndex = 8;
            this.buttonKeyBoard18.Tag = "1";
            this.buttonKeyBoard18.Text = "1";
            this.buttonKeyBoard18.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard20
            // 
            this.buttonKeyBoard20.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard20.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard20.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard20.Location = new System.Drawing.Point(9, 166);
            this.buttonKeyBoard20.Name = "buttonKeyBoard20";
            this.buttonKeyBoard20.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard20.TabIndex = 8;
            this.buttonKeyBoard20.Tag = "0";
            this.buttonKeyBoard20.Text = "0";
            this.buttonKeyBoard20.UseVisualStyleBackColor = false;
            // 
            // btn_Enter
            // 
            this.btn_Enter.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_Enter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Enter.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.btn_Enter.ForeColor = System.Drawing.Color.White;
            this.btn_Enter.Location = new System.Drawing.Point(177, 114);
            this.btn_Enter.Name = "btn_Enter";
            this.btn_Enter.Size = new System.Drawing.Size(50, 98);
            this.btn_Enter.TabIndex = 8;
            this.btn_Enter.Tag = "{ENTER}";
            this.btn_Enter.Text = "回车";
            this.btn_Enter.UseVisualStyleBackColor = false;
            this.btn_Enter.Click += new System.EventHandler(this.btn_Enter_Click);
            // 
            // buttonKeyBoard22
            // 
            this.buttonKeyBoard22.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard22.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard22.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard22.Location = new System.Drawing.Point(65, 114);
            this.buttonKeyBoard22.Name = "buttonKeyBoard22";
            this.buttonKeyBoard22.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard22.TabIndex = 8;
            this.buttonKeyBoard22.Tag = "2";
            this.buttonKeyBoard22.Text = "2";
            this.buttonKeyBoard22.UseVisualStyleBackColor = false;
            // 
            // buttonKeyBoard23
            // 
            this.buttonKeyBoard23.BackColor = System.Drawing.Color.Gray;
            this.buttonKeyBoard23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonKeyBoard23.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.buttonKeyBoard23.ForeColor = System.Drawing.Color.White;
            this.buttonKeyBoard23.Location = new System.Drawing.Point(121, 114);
            this.buttonKeyBoard23.Name = "buttonKeyBoard23";
            this.buttonKeyBoard23.Size = new System.Drawing.Size(50, 46);
            this.buttonKeyBoard23.TabIndex = 8;
            this.buttonKeyBoard23.Tag = "3";
            this.buttonKeyBoard23.Text = "3";
            this.buttonKeyBoard23.UseVisualStyleBackColor = false;
            // 
            // btn_Delete
            // 
            this.btn_Delete.BackColor = System.Drawing.Color.DarkOrange;
            this.btn_Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Delete.Font = new System.Drawing.Font("微软雅黑", 15F);
            this.btn_Delete.ForeColor = System.Drawing.Color.White;
            this.btn_Delete.Location = new System.Drawing.Point(65, 166);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(106, 46);
            this.btn_Delete.TabIndex = 8;
            this.btn_Delete.Tag = "{BKSP}";
            this.btn_Delete.Text = "删除";
            this.btn_Delete.UseVisualStyleBackColor = false;
            // 
            // HoverNumPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 227);
            this.Controls.Add(this.buttonKeyBoard13);
            this.Controls.Add(this.buttonKeyBoard12);
            this.Controls.Add(this.buttonKeyBoard9);
            this.Controls.Add(this.btn_Enter);
            this.Controls.Add(this.buttonKeyBoard17);
            this.Controls.Add(this.buttonKeyBoard23);
            this.Controls.Add(this.buttonKeyBoard16);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.buttonKeyBoard20);
            this.Controls.Add(this.buttonKeyBoard22);
            this.Controls.Add(this.buttonKeyBoard18);
            this.Controls.Add(this.buttonKeyBoard15);
            this.Controls.Add(this.buttonKeyBoard14);
            this.Controls.Add(this.button13);
            this.Name = "HoverNumPad";
            this.ResumeLayout(false);

        }

        #endregion

        private ButtonKeyBoard button1;
        private ButtonKeyBoard button2;
        private ButtonKeyBoard button3;
        private ButtonKeyBoard ButtonKeyBoard1;
        private ButtonKeyBoard ButtonKeyBoard2;
        private ButtonKeyBoard ButtonKeyBoard3;
        private ButtonKeyBoard ButtonKeyBoard4;
        private ButtonKeyBoard ButtonKeyBoard5;
        private ButtonKeyBoard ButtonKeyBoard6;
        private ButtonKeyBoard ButtonKeyBoard7;
        private ButtonKeyBoard ButtonKeyBoard8;
        private ButtonKeyBoard btn_Clear;
        private ButtonKeyBoard ButtonKeyBoard10;
        private ButtonKeyBoard ButtonKeyBoard11;
        private ButtonKeyBoard button13;
        private ButtonKeyBoard buttonKeyBoard9;
        private ButtonKeyBoard buttonKeyBoard12;
        private ButtonKeyBoard buttonKeyBoard13;
        private ButtonKeyBoard buttonKeyBoard14;
        private ButtonKeyBoard buttonKeyBoard15;
        private ButtonKeyBoard buttonKeyBoard16;
        private ButtonKeyBoard buttonKeyBoard17;
        private ButtonKeyBoard buttonKeyBoard18;
        private ButtonKeyBoard buttonKeyBoard20;
        private ButtonKeyBoard btn_Enter;
        private ButtonKeyBoard buttonKeyBoard22;
        private ButtonKeyBoard buttonKeyBoard23;
        private ButtonKeyBoard btn_Delete;
    }
}