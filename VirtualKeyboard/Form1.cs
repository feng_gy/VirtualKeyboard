﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VirtualKeyboard
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                texBoxWKeyBoard1.TextBox.TextEntryMode = TextBoxWKeyBoard.TexBoxWKeyBoard.EntryMode.Numeric;
            }
            else
            {
                texBoxWKeyBoard1.TextBox.TextEntryMode = TextBoxWKeyBoard.TexBoxWKeyBoard.EntryMode.Standard;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.textBoxKeyBoard1.EntryMode = this.checkBox2.Checked ? NewTextBoxKeyBoard.EntryMode.Numeric : NewTextBoxKeyBoard.EntryMode.Standard;
        }
    }
}
